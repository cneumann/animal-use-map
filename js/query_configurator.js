var QueryConfigurator = (function() {
  function C() { constructor.apply(this, arguments); };
  var p = C.prototype;

  p.queries = [];

  function constructor(wrapid,queries) {
    this.queries = queries;
    this.queriesWrap = jQuery('#'+wrapid);
    this.init();
  };

  p.init = function() {
    var self = this;
    this.queries.forEach(function(element,catIndex) {
      self.queriesWrap.append(jQuery('<h3 class="mt-3 mb-2">' + element.name + '</h3>'));
      element.queries.forEach(function(element,queryIndex) {
        var queryCheckbox = jQuery(
          '<div><label><input type="checkbox" '
            + 'data-query-index="' + queryIndex + '" '
            + 'data-category-index="' + catIndex + '" '
            + '> ' + element.name
            + (element.description ? ' (' + element.description + ')' : '')
            + '</label></div>');
        self.queriesWrap.append(queryCheckbox);
      });
    });
  };
  return C;
}());

export default QueryConfigurator;
