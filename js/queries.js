export default [
  {
    name: "Food",
    queries: [
      {
        name: 'Farm yards',
        description: 'All farmyards (with or without animals)',
        tags: [
          [{landuse: 'farmyard'}]
        ]
      },
      {
        name: 'Feedlots (mostly in US)',
        tags: [
          [{farmyard: 'feedlot'}]
        ]
      },
      {
        name: 'Slaughterhouses (few mapped)',
        tags: [
          [{industrial: 'slaughterhouse'}]
        ]
      },
      {
        name: 'Meat production (few mapped)',
        tags: [
          [{product: 'meat'}]
        ]
      },
      {
        name: 'Butcher',
        tags: [
          [{shop: 'butcher'}]
        ]
      },
      {
        name: 'Fishing',
        tags: [
          [{leisure: 'fishing'}]
        ]
      },
      {
        name: 'Bee hives',
        tags: [
          [{man_made: 'beehive'}]
        ]
      },
    ],
  },
  {
    name: "Clothing",
    queries: [
      {
        name: 'Leather shops',
        tags: [
          [{shop: 'leather'}]
        ]
      },
    ],
  },
  {
    name: "Entertainment",
    queries: [
      {
        name: 'Dog racing',
        tags: [
          [{sport: 'dog_racing'}]
        ]
      },
      {
        name: 'Zoos',
        tags: [
          [{tourism: 'zoo'}],
          [{zoo: true}],
        ]
      },
      {
        name: 'Attractions',
        tags: [
          [{attraction: 'animal'}],
        ]
      },
      {
        name: 'Aquarism',
        tags: [
          [{tourism: 'aquarium'}],
          [{zoo: true}],
        ]
      },
      {
        name: 'Equestrian clubs',
        tags: [
          [{club: 'equestrian'}]
        ]
      },
      {
        name: 'Bridleways',
        description: 'A route or way intended for horse riding',
        tags: [
          [{highway: 'bridleway'}],
          [{route: 'horse'}]
        ]
      },
      {
        name: 'Horse sports',
        tags: [
          [{sport: 'equestrian'}],
          [{sport: 'horse_racing'}]
        ]
      },
      {
        name: 'Horse riding',
        tags: [
          [{leisure: 'horse_riding'}]
        ]
      },
      {
        name: 'Bull fighting',
        tags: [
          [{sport: 'bullfight'}]
        ]
      },
    ],
  },
  {
    name: "Experimentation",
    queries: [
    ],
  },
  {
    name: "Animal companions",
    queries: [
      {
        name: 'Defect: Animal training',
        tags: [
          [{amenity: '~_training$'}],
          [{animal_training: true}],
          [{sport: '~_training$'}],
        ]
      },
      {
        name: 'Pet shops',
        tags: [
          [{shop: 'pet'}]
        ]
      },
      {
        name: 'Boarding',
        tags: [
          [{amenity: 'animal_boarding'}],
          [{animal_boarding: true}]
        ]
      },
      {
        name: 'Dog clubs',
        tags: [
          [{club: 'dog'}]
        ]
      },
      {
        name: 'Wellness',
        tags: [
          [{animal: 'wellness'}]
        ]
      },
    ],
  },
  {
    name: "Common",
    queries: [
      {
        name: 'Cemeteries',
        tags: [
          [{animal: 'cemetery'}],
        ]
      },
      {
        name: 'Crematoriums',
        tags: [
          [{animal: 'crematorium'}],
        ]
      },
      {
        name: 'Shelters',
        tags: [
          [{amenity: 'animal_shelter'}],
          [{animal_shelter: true}]
        ]
      },
      {
        name: 'Breeding',
        tags: [
          [{amenity: 'animal_breeding'}],
          [{animal_breeding: true}]
        ]
      },
      {
        name: 'Veterinaries',
        tags: [
          [{amenity: 'veterinary'}]
        ]
      },
    ],
  },
  {
    name: "Miscellaneous",
    queries: [
      {
        name: 'Hunting stands',
        tags: [
          [{amenity: 'hunting_stand'}]
        ]
      },
    ],
  },
];
