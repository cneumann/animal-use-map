"use strict";

import queries from './js/queries.js';
import Map from './js/map.js';
import QueryConfigurator from './js/query_configurator.js';
import LoadingModal from './js/loading_modal.js';

var map = new Map({
  mapId: 'main-map',
  loadingModal: new LoadingModal(jQuery('.loading-modal')),
  queryConfigurator: new QueryConfigurator('queries',queries),
});

jQuery('.query-button').on('click', function(event) {
  map.refreshData(queries);
});

jQuery('.sidebar-toggle').on('click', function(event) {
  event.preventDefault();
  jQuery('.main-container').toggleClass('main-container--sidebar-hidden');
  setTimeout(function(){ map.map.invalidateSize(); }, 500);

});

